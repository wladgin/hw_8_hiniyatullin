<?php
/**
 * Created by PhpStorm.
 * User: WladGita
 * Date: 09.01.2018
 * Time: 21:38
 */
require_once 'Classes/Hotel.php';
require_once 'hotels.php';

$hotelIndex = $_GET['id'];

if (isset($hotels[$hotelIndex])) {
    $viewedHotels = $_COOKIE['viewed'];
    if ($viewedHotels) {
        $viewedHotels = json_decode($viewedHotels, true);
    }
    if (is_array($viewedHotels)) {
        if (!in_array($hotelIndex, $viewedHotels)) {
            $viewedHotels[] = $hotelIndex;
        }
    } else {
        $viewedHotels = [$hotelIndex];
    }
    setcookie('viewed', json_encode($viewedHotels), time()+3600*24*30);
    echo $hotels[$hotelIndex]->writeAsHtmlSecodPage();

echo '<div>' . '<h2 align="center">' .'Просмотренные отели' . '</h2>';

    foreach ($viewedHotels as $viewedId) {
        if ($hotelIndex != $viewedId) {
            echo  $hotels[$viewedId]->title . '<br>';
        }
    }
} else {
    echo "Страница не найдена!";
}

echo '</div>';
