<?php


class Hotel
{
    public $title;
    public $shortDescription;
    public $description;
    public $href;

    /**
     * Hotel constructor.
     * @param $title
     * @param $shortDescription
     * @param $description
     */
    public function __construct($title, $shortDescription, $description)
    {
        $this->title = $title;
        $this->shortDescription = $shortDescription;
        $this->description = $description;
    }

    public function writeAsHtml($index)
    {
        $resultString = '<div>';
        $resultString .= '<h2>' . $this->title . '</h2>';
        $resultString .= '<p>' . $this->shortDescription . '</p>';
        $resultString .= "<a href='view.php?id=$index'>Подробнее</a>";
        $resultString .= '</div>';
        return $resultString;
    }

    public function writeAsHtmlSecodPage(){
        $resultString = '<div>';
        $resultString .= '<h2>' . $this->title . '</h2>';
        $resultString .= '<p>' . $this->description . '</p>';
        $resultString .= '</div>';
        return $resultString;
    }

}