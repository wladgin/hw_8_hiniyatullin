<?php
require_once 'Classes/Hotel.php';
require_once 'hotels.php';


for($i=0; $i<count($hotels); $i++){
    echo $hotels[$i]->writeAsHtml($i);
}

$viewedHotels = $_COOKIE['viewed'];
if ($viewedHotels) {
    $viewedHotels = json_decode($viewedHotels, true);
}

echo '<div>' . '<h2 align="center">' .'Просмотренные отели' . '</h2>';

if (is_array($viewedHotels)) {
    foreach ($viewedHotels as $viewedId) {
        echo $hotels[$viewedId]->title . '<br>';
    }
}

echo '</div>';
